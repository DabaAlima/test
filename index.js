const fs = require('fs')
const csv = require('csv-parser')
const math = require('mathjs')
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})
const monetaryDebts = [];

//Takes and array and write the summarized data into a new csv file
function writeToCSVFile(debts, filename) {

    fs.writeFile(filename, extractAsCSV(debts), err => {
        if (err) {
            console.log('Error writing to csv file', err);
        } else {
            console.log(`Data is summarized and saved in the folder of this project as ${filename}`);
        }
    });
}

//Takes and array and generates another array where each element is a string containing comma-separated values for a row
function extractAsCSV(debts) {
    const header = ["Debtor,Creditor,Amount"];
    const rows = debts.map(debt =>
        `${debt.debtor},${debt.creditor},${debt.amount}`
    );
    return header.concat(rows).join("\n");
}

readline.question(`Please provide the name or the path of the .CSV file, without the extension: `, filename => {
    const path = filename + `.csv`;

    fs.access(path, fs.F_OK, (err) => { //Check if the file exist, without opening it

        if (err) {
            console.error(err)
            return
        } // If the file provided does not exist, return an error

        const outPutFile = filename + `-summarized.csv`; //Generate the name of the output file
        fs.createReadStream(filename + `.csv`)
            .pipe(csv())
            .on('data', function (row) {
                const monetaryDebt = {
                    debtor: row.Debtor,
                    creditor: row.Creditor,
                    amount: parseFloat(row.Amount)
                }
                monetaryDebts.push(monetaryDebt) //Create an array containing each line of the CSV file

                //Compare each element of the array with the followings and add the amount if the 2 names match, then delete any macthing element
                monetaryDebts.forEach(monetaryDebt => {
                    let getItsIndex = monetaryDebts.indexOf(monetaryDebt);

                    for (let i = ++getItsIndex; i < monetaryDebts.length; i++) {
                        if (monetaryDebt.debtor === monetaryDebts[i].debtor && monetaryDebt.creditor === monetaryDebts[i].creditor) {
                            monetaryDebts[getItsIndex - 1].amount = math.round(monetaryDebt.amount + monetaryDebts[i].amount, 2);
                            monetaryDebts.splice(i, 1); // Remove any matching element from the initial array
                        }
                    }
                })
            })
            .on('end', function () {
                writeToCSVFile(monetaryDebts, outPutFile) //Save summarized data in a new file
            })

    })
    readline.close()
})